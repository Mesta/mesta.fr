var gulp		= require('gulp'),
	sass		= require('gulp-sass');
var browserSync;

gulp.task('default', function() {
});

gulp.task('sass', function () {
	browserSync = require('browser-sync').create();

	return gulp.src('./sass/**/*.scss')
		.pipe(sass({
			'includePaths': require("bourbon").includePaths,
		})
		.on('error', sass.logError))
		.pipe(gulp.dest('./css'))
		.pipe(browserSync.stream());
});

gulp.task('watch', function () {
	browserSync = require('browser-sync').create();
	browserSync.init({
        server: "./"
    });
	gulp.watch('./sass/**/*.scss', ['sass']);
});

gulp.task('deploy:production', function(){
	return gulp.src('./sass/**/*.scss')
		.pipe(sass({
			'includePaths': require("bourbon").includePaths,
		})
		.on('error', sass.logError))
		.pipe(gulp.dest('./css'));
})
